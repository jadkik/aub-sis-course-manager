import sys

__version__ = '0.5'
__doc__ = '''
AUBsis Calendar app.
There is nothing here for the moment.
'''

from argparse import _SubParsersAction as SPA, ArgumentParser as AP, \
    SUPPRESS, REMAINDER, PARSER, FileType
import getpass

class ArgumentParser(AP):
    """
    Patch the built-in ArgumentParser because it does not accept
    default subparsers.
    It has been fixed: http://bugs.python.org/issue9253
    For the time being, we'll stick to this.
    Because the patch has been merged to python3.3 only.
    """
    def _get_nargs_pattern(self, action):
        if action.nargs == PARSER and action.default is not None:
            return '(-*A?[-AO]*)'
        else:
            return super(ArgumentParser, self)._get_nargs_pattern(action)
    
    def _get_values(self, action, arg_strings):
        if action.nargs == PARSER and action.default is not None:
            if len(arg_strings) == 0:
                value = [action.default]
            else:
                value = [self._get_value(action, v) for v in arg_strings]
            self._check_value(action, value[0])
            return value
        else:
            return super(ArgumentParser, self)._get_values(action, arg_strings)

def run():
    args = ArgumentParser()
    
    # Database connection settings
    args.add_argument('--db-driver', dest="db_driver", default="sqlite")
    args.add_argument('--db-host', dest="db_host", default=None)
    args.add_argument('--db-user', dest="db_user", default=None)
    args.add_argument('--db-pass', dest="db_password", default=None)
    args.add_argument('--db-name', dest="db_name", default="aubsis.sqlite")
    args.add_argument('--db-echo', dest="db_echo", action="store_true")
    
    subparsers = args.add_subparsers(
        dest='command',
        metavar='command',
    )
    subparsers.default = "interactive"
    
    # interactive
    interactive_parser = subparsers.add_parser('interactive', help='Interactive interface')
    
    # scrape -t {term} -u {user_id} -p {password} -s {courses} -c {courses_dir}
    scrape_parser = subparsers.add_parser('scrape', help='Parse directly from files')
    scrape_parser.add_argument('-t --term', dest='term', type=unicode,
                            help="Term to search in",
                            required=True)
    scrape_parser.add_argument('-u --user', dest='user_id', type=unicode,
                            help="AUB SIS User ID",
                            required=True)
    scrape_parser.add_argument('-p --password', dest='password', type=unicode,
                            help="AUB SIS Password", default=None,
                            required=False)
    scrape_parser.add_argument('-s --search', dest='courses', type=unicode,
                            help="Courses to search for. Example: `EECE 3:SOAN:PHYS 210L` for all EECE 3xx courses, all SOAN courses and the PHYS 210L course.",
                            required=True)
    scrape_parser.add_argument('-c --courses-dir', dest='courses_dir', type=unicode,
                            help="Courses directory",
                            required=True)
    
    # parse -c {courses_dir}
    parse_parser = subparsers.add_parser('parse', help='Parse directly from files')
    parse_parser.add_argument('-c --courses-dir', dest='courses_dir', type=unicode,
                            help="Courses directory",
                            required=True)
    
    # update -c {courses_dir}
    update_parser = subparsers.add_parser('update', help='Update database')
    update_parser.add_argument('-c --courses-dir', dest='courses_dir', type=unicode,
                            help="Courses directory",
                            required=True)
    
    # clear
    clear_parser = subparsers.add_parser('clear', help='Clear database')
    
    # schedule -r {rules} -l {load} -s {save}
    schedule_parser = subparsers.add_parser('schedule', help='Schedule courses')
    schedule_parser.add_argument('-r --rules', dest='rules', type=FileType('r'),
                                help='Rules file',
                                default=None)
    schedule_parser.add_argument('-l --load', dest='load', type=FileType('r'),
                                help='Load saved schedule',
                                default=None)
    schedule_parser.add_argument('-s --save', dest='save', type=FileType('w'),
                                help='Save schedule in the end',
                                default=None)
    
    # calendar -r {rules} -l {load} -s {save}
    calendar_parser = subparsers.add_parser('calendar', help='Generate Calendar for Schedule')
    calendar_parser.add_argument('-s --schedule', dest='schedule_id', type=unicode,
                                help='Schedule ID',
                                required=True)
    calendar_parser.add_argument('-t --title', dest='title', type=unicode,
                                help='Set the title of the file',
                                default="Schedule")
    calendar_parser.add_argument('-o --output', dest='output', type=unicode,
                                help='Set PDF filename for output',
                                required=True)
    calendar_parser.add_argument('-a --author', dest='author', type=unicode,
                                help='Set the author of the file',
                                default="AUB sis")
    calendar_parser.add_argument('-l --locale', dest='locale', type=unicode,
                                help='Set the locale/language of the document',
                                default="en_US")
    calendar_parser.add_argument('-p --pagesize', dest='pagesize', type=unicode,
                                help='Set PDF pagesize', choices=("A4", "letter"),
                                required=False, default="A4")
    calendar_parser.add_argument('-r --orientation', dest='orientation', type=unicode,
                                help='Set PDF orientation', choices=("portrait", "landscape"),
                                required=False, default="portrait")
    calendar_parser.add_argument('-g --grayscale', dest='grayscale',
                                help='Use colors or black and white only',
                                action="store_true")
    calendar_parser.add_argument('-d --days', dest='days',
                                help='What days and order of days to display (UMTWRFS)',
                                default=None)
    calendar_parser.add_argument('--view', dest='open_native',
                                help='View the file once it is generated',
                                action="store_true")
    
    # export -s {schedule_id} -o {output}
    export_parser = subparsers.add_parser('export', help='Export Calendar for Schedule in iCalendar format')
    export_parser.add_argument('-s --schedule', dest='schedule_id', type=unicode,
                                help='Schedule ID',
                                required=True)
    export_parser.add_argument('-o --output', dest='output', type=unicode,
                                help='Set ICS filename for output',
                                required=True)
    export_parser.add_argument('-t --timezone', dest='timezone', type=unicode,
                                help='Timezone or country code to use',
                                required=False, default=None)
    
    #
    # Do the thing
    ns = args.parse_args(sys.argv[1:])
    
    from aubsis import database
    database.connect(ns)
    
    from aubsis import models
    
    if ns.command == "interactive":
        from aubsis.interactive import main
        main(ns)
    elif ns.command == "scrape":
        from aubsis.sel_scraper import main
        if not ns.password:
			ns.password = getpass.getpass()
        main(ns)
    elif ns.command == "parse":
        from aubsis.parser import main
        main(ns)
    elif ns.command == "update":
        from aubsis.parser import update_database
        update_database(ns)
    elif ns.command == "clear":
        from aubsis.database import create
        create(ns)
    elif ns.command == "schedule":
        from aubsis.schedule import main
        main(ns)
    elif ns.command == "calendar":
        from aubsis.calendar import main
        main(ns)
    elif ns.command == "export":
        from aubsis.export import main
        main(ns)
    else:
        raise ValueError("something is not right")
