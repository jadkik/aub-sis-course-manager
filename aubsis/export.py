from icalendar import Calendar, Event
import pytz
import datetime as dt
import uuid

import aubsis
from aubsis.models import Schedule

def generate_calendar(schedule, filename=None, timezone=None):
    cal = Calendar()

    cal.add('prodid', '-//AUBsis Calendar//aubsis.export//EN')
    cal.add('version', aubsis.__version__)

    """
    BEGIN:VEVENT
    CREATED;VALUE=DATE-TIME:20130829T202533Z
    UID:83a4202187
    RRULE:FREQ=WEEKLY;BYDAY=MO,WE,FR;INTERVAL=1;UNTIL=20131221
    LAST-MODIFIED;VALUE=DATE-TIME:20130829T202630Z
    DTSTAMP;VALUE=DATE-TIME:20130829T202630Z
    SUMMARY:EECE 310
    DTSTART;VALUE=DATE-TIME:20130904T060000Z
    DTEND;VALUE=DATE-TIME:20130904T065000Z
    CLASS:PUBLIC
    LOCATION:BECHTA 405
    DESCRIPTION:EECE 310-1\n10993 Class\n9:00 am-9:50 am\nBECHTA 405
    END:VEVENT
    """
    utc = pytz.utc

    day_map = {"U": "SU", "M": "MO", "T": "TU", "W": "WE", "R": "TH", "F": "FR", "S": "SA"}
    day_index = "MTWRFSU"

    for p in schedule.periods:
        start_date = dt.datetime(year=p.start_date.year, month=p.start_date.month, day=p.start_date.day)
        
        weekdays = [day_index.index(d) for d in p.days]
        start_weekday = start_date.weekday()
        try:
            first_weekday = (d for d in weekdays if d >= start_weekday).next()
        except StopIteration:
            first_weekday = min(weekdays)
        first_date = start_date + dt.timedelta( (first_weekday-start_date.weekday()) % 7 )
        
        start_date = max(first_date, start_date)
        
        dtstart = start_date.replace(hour=p.start_time.hour, minute=p.start_time.minute, second=p.start_time.second, tzinfo=timezone)
        dtend = start_date.replace(hour=p.end_time.hour, minute=p.end_time.minute, second=p.end_time.second, tzinfo=timezone)
        
        summary = p.course.name
        
        description = """
{c.name}-{c.section}
{c.crn} Class
{p.start_time:%H:%M %p}-{p.end_time:%H:%M %p}
{p.location}
w/ {instructors}
""".format(c=p.course, p=p, instructors=(', '.join(p.instructors) or '[N/A]')).strip()
        now = dt.datetime.now()

        event = Event()
        
        event.add('uid', '{}/{}@aubsis'.format(uuid.uuid1(), p.course.crn))
        event.add('summary', summary)
        event.add('description', description)
        event.add('dtstart', dtstart)
        event.add('dtend', dtend)
        event.add('dtstamp', now)
        event.add('created', now)
        event.add('last-modified', now)
        event.add('location', p.location)
        event.add('class', 'PUBLIC')
        event.add('rrule', {'FREQ': 'WEEKLY', 'BYDAY': [day_map[d] for d in p.days], 'INTERVAL': 1, 'UNTIL': p.end_date})

        cal.add_component(event)
    
    if filename is not None:
        with open(filename, 'wb') as f:
            f.write(cal.to_ical())
    
    return cal

def main(args):
    session = aubsis.database.session()
    
    if args.timezone is None:
        tz = None
    elif len(args.timezone) == 2 and args.timezone in pytz.country_timezones:
        tz = pytz.timezone(pytz.country_timezones[args.timezone][0])
    else:
        # May raise an exception
        tz = pytz.timezone(args.timezone)
    
    sch = session.query(Schedule).filter_by(id=args.schedule_id).one()
    cal = generate_calendar(
        filename=args.output,
        schedule=sch,
        timezone=tz,
    )
