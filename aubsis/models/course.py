import aubsis.database

from sqlalchemy import func, Table, Column, Integer, String, Float, Boolean, MetaData, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method, Comparator

class Course(aubsis.database.Base):
    __tablename__ = 'courses'

    id = Column(Integer, primary_key=True)
    crn = Column(Integer, nullable=False)
    subject = Column(String(255), nullable=False)
    title = Column(String(255), nullable=False)
    number = Column(String(4), nullable=False)
    section = Column(Integer, nullable=False)

    @hybrid_property
    def name(self):
        return '{} {}'.format(self.subject, self.number)

    def __repr__(self):
        return '<Course {}/{} {}/{}>'.format(self.crn, self.subject, self.number, self.section)
