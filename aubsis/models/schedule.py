import aubsis.database

from sqlalchemy import func, Table, Column, Integer, String, Float, Boolean, MetaData, ForeignKey, select
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method, Comparator
from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound

import hashlib

schedule_course = Table('schedule_course', aubsis.database.Base.metadata,
    Column('schedule_id', String(32), ForeignKey('schedules.id')),
    Column('course_id', Integer, ForeignKey('courses.id')),
)

class Schedule(aubsis.database.Base):
    __tablename__ = 'schedules'

    id = Column(String(32), primary_key=True)

    courses = relationship("Course", secondary=schedule_course)

    periods = relationship("Period", primaryjoin= "schedules.c.id==schedule_course.c.schedule_id",
                                     secondaryjoin= "periods.c.course_id==schedule_course.c.course_id",
                                     secondary=schedule_course)

    @classmethod
    def hash(cls, courses):
        hsh = hashlib.md5(":".join(sorted(unicode(c.crn) for c in courses)))
        sid = hsh.hexdigest()
        return sid
    
    @classmethod
    def check(cls, courses):
        sid = cls.hash(courses)
        session = aubsis.database.session()
        try:
            session.query(Schedule).filter_by(id=sid).one()
        except NoResultFound as e:
            return None
        except: # MultipleResultsFound
            raise
        else:
            return sid
    
    def __init__(self, courses):
        sid = Schedule.hash(courses)
        self.id = sid
        self.courses = courses

    @hybrid_property
    def name(self):
        return 'Schedule #{}'.format(self.id[:7])

    def __repr__(self):
        return '<Schedule {}>'.format(self.id)
