
from .instructor import *
from .course import *
from .period import *
from .schedule import *
