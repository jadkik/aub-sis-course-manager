import aubsis.database

from sqlalchemy import func, Table, Column, Integer, String, Float, Boolean, Time, Date, MetaData, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method, Comparator
from sqlalchemy.ext.associationproxy import association_proxy

period_instructors = Table('period_instructor', aubsis.database.Base.metadata,
    Column('period_id', Integer, ForeignKey('periods.id')),
    Column('instructor_id', Integer, ForeignKey('instructors.id'))
    )

class Period(aubsis.database.Base):
    __tablename__ = 'periods'

    id = Column(Integer, primary_key=True)
    course_id = Column(Integer, ForeignKey('courses.id'), nullable=False)
    days = Column(String(5), nullable=False, default='')
    start_time = Column(Time, nullable=True)
    end_time = Column(Time, nullable=True)
    location = Column(String(255), nullable=False, default='TBA')
    
    start_date = Column(Date, nullable=True)
    end_date = Column(Date, nullable=True)
    
    ins = relationship("Instructor", secondary=period_instructors, backref="periods")
    instructors = association_proxy('ins', 'name')
    
    course = relationship("Course", backref="periods")

    def __repr__(self):
        return '<Period {} {}>'.format(self.days, self.start_time)
