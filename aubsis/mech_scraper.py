import re, time
import mechanize

DELAY = 3

br = mechanize.Browser()
br.addheaders = [('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:19.0) Gecko/20100101 Firefox/19.0')]
br.open("https://www.aub.edu.lb/banner/")

time.sleep(DELAY)

response1 = br.follow_link(text="Enter Secure Area")
assert br.viewing_html()
assert br.title() == 'User Login', br.title()

time.sleep(DELAY)

print 'before login'

br.select_form(name="loginform")
br["sid"] = "201300348"
br["PIN"] = "198256"
response2 = br.submit()
r = response2.read()

assert 'Jad W. El Kik' in r, r

time.sleep(DELAY)
assert br.title() == 'Main Menu', br.title()

print 'after login'

br.follow_link(text="Student Services")
time.sleep(DELAY)
assert br.title() == 'Student Services', br.title()

print 'student services'

br.follow_link(text="Registration")
time.sleep(DELAY)
assert br.title() == 'Registration', br.title()

print 'registration'

br.follow_link(text="Look-up Classes to Add")
time.sleep(DELAY)
assert br.title() == 'Select Term or Date Range', br.title()

print 'select term'

br.select_form(nr=1)
br["p_term"] = ["201320"]
br.submit()

time.sleep(DELAY)
assert br.title() == 'Look-Up Classes to Add JWF', br.title()

print 'after select term'

br.select_form(nr=1)
br.submit(name='SUB_BTN', label='Advanced Search')

time.sleep(DELAY)
assert br.title() == 'Advanced Search', br.title()

br.select_form(nr=1)
br['sel_subj'] = ['CHEM']
br['sel_crse'] = '203'
response3 = br.submit()

time.sleep(DELAY)
assert br.title() == 'Look-Up Classes to Add:', br.title()

with open('file.txt', 'w') as f:
    f.write(response3.read())
