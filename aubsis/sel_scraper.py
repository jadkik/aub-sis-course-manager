from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait as WDW # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

from time import sleep
from random import choice
import codecs, os

import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

wait_time = range(-1, 1)
def WebDriverWait(d, t):
    #sleep(t/2.0 + choice(wait_time)/2.0)
    return WDW(d, t)

class SeleniumScraper(object):
    MAX_WAIT = 10
    
    HOMEPAGE, LOGIN, MAIN_MENU, STUDENT_SERVICES, REGISTRATION, SELECT_TERM, \
        LOOK_UP, ADVANCED_SEARCH, SEARCH_RESULTS = range(9)
    
    def __init__(self, term, user_id, password):
        # Create a new instance of the Firefox driver
        self.driver = webdriver.Chrome()
        self.driver.implicitly_wait(5)
        
        self.aubsis_user_id = user_id
        self.aubsis_password = password
        self.target_term = term
        
        self.__page = None

    def login(self):
        logger.info('Go to banner')
        
        self.driver.get("http://www.aub.edu.lb/banner/")

        WebDriverWait(self.driver, self.MAX_WAIT).until(EC.title_contains("homepage"))

        self.__page = self.HOMEPAGE

        logger.debug('In banner')
        logger.info('Go to login')
        
        self.driver.find_element_by_link_text("Enter Secure Area").click()

        WebDriverWait(self.driver, self.MAX_WAIT).until(EC.title_contains("User Login"))

        self.__page = self.LOGIN

        logger.info('Login using credentials: ' + self.aubsis_user_id)

        idElement = self.driver.find_element(By.NAME, "sid")
        idElement.send_keys(self.aubsis_user_id)

        pinElement = self.driver.find_element(By.NAME, "PIN")
        pinElement.send_keys(self.aubsis_password)

        logger.debug("Filled form")

        pinElement.submit()
        
        logger.debug("Submitted form")
        
        WebDriverWait(self.driver, self.MAX_WAIT).until(EC.title_contains("Main Menu"))
        
        self.__page = self.MAIN_MENU
        
        logger.debug("Done logging in")

    def gotoStudentServices(self):
        
        logger.info('Go to Student Services')
        
        self.driver.find_element(By.PARTIAL_LINK_TEXT, "Student Services").click()

        WebDriverWait(self.driver, self.MAX_WAIT).until(EC.title_contains("Student Services"))
        
        self.__page = self.STUDENT_SERVICES
        
        logger.debug('In Student Services')
        
    def gotoRegistration(self):
        logger.info('Go to Registration')
        
        self.driver.find_element(By.PARTIAL_LINK_TEXT, "Registration").click()

        WebDriverWait(self.driver, self.MAX_WAIT).until(EC.title_contains("Registration"))

        self.__page = self.REGISTRATION
        
        logger.debug('In Registration')

    def gotoLookUp(self):
        logger.info('Go to Look Up')
        
        self.driver.find_element(By.PARTIAL_LINK_TEXT, "Look-up Classes to Add").click()

        WebDriverWait(self.driver, self.MAX_WAIT).until(EC.title_contains("Select Term"))
        
        self.__page = self.SELECT_TERM
        
        logger.debug('In Look Up')
        
        self.subSelectTerm()

    def subSelectTerm(self):
        logger.debug('Sub Select Term')
        
        selectElement = self.driver.find_element(By.NAME, "p_term")
        
        logger.debug('Select Element is %s', selectElement)
        
        select = Select(selectElement)
        
        logger.debug('Select is %s', select)
        
        #select.deselect_all()
        index = 0
        for i, option in enumerate(select.options):
            if option.text.startswith(self.target_term):
                index = i
                logger.debug('Selected option %s: %r needed %r', i, option.text, self.target_term)
                break
        else:
            logger.fatal('Cannot find a term that starts with: %r', self.target_term)
            return
        select.select_by_index(index)
        
        #select.select_by_visible_text("Fall 2013-2014")
        
        logger.debug('Submitting form')
        
        selectElement.submit()
        
        logger.debug('Submitted form')
        
        WebDriverWait(self.driver, self.MAX_WAIT).until(EC.title_contains("Look-Up Classes to Add"))
        
        self.__page = self.LOOK_UP
        
        logger.debug('Done selecting term')

    def gotoAdvancedSearch(self):
        logger.info('Go to Advanced Search')
        
        self.driver.find_element(By.CSS_SELECTOR, "input[value='Advanced Search']").click()

        WebDriverWait(self.driver, 10).until(EC.title_contains("Advanced Search"))
        
        self.__page = self.ADVANCED_SEARCH
        
        logger.debug('In Advanced Search')

    def doSearch(self, subject, number):
        logger.info('Go to Search for %s %s', subject, number)
        
        subjElement = self.driver.find_element(By.ID, "subj_id")
        
        logger.debug('Subject Element is %s', subjElement)
        
        select = Select(subjElement)
        
        logger.debug('Select is %s', select)
        
        select.deselect_all()
        if subject == 'ALL':
            for option in select.options:
                select._setSelected(option)
        else:
            select.select_by_value(subject)

        crseElement = self.driver.find_element(By.ID, "crse_id")
        
        logger.debug('Course Element is %s', crseElement)
        
        crseElement.send_keys(number)

        self.driver.find_element(By.CSS_SELECTOR, "input[value='Section Search']").click()
        
        WebDriverWait(self.driver, 10).until(EC.title_contains("Look-Up Classes to Add"))
        
        self.__page = self.SEARCH_RESULTS
        
        logger.debug('In search results')

    def newSearch(self):
        logger.info('Go to New Search')
        
        self.driver.find_element(By.CSS_SELECTOR, "input[value='New Search']").click()
        
        WebDriverWait(self.driver, 10).until(EC.title_contains("Select Term"))
        
        self.__page = self.SELECT_TERM
        
        logger.debug('Back to new search')
        
        self.subSelectTerm()
        
        self.gotoAdvancedSearch()

    def saveSearch(self, subject, number, filename):
        with codecs.open(filename, 'w', 'utf-8') as f:
            self.doSearch(subject, number)
            source = self.driver.page_source
            
            logger.info('Saving search to %s', filename)
            f.write(source)

    def searchAndSave(self, subject, number, directory):
        
        if self.__page == self.SEARCH_RESULTS:
            self.newSearch()
        elif self.__page == self.ADVANCED_SEARCH:
            pass
        else:
            # Start all over again
            self.login()
            self.gotoStudentServices()
            self.gotoRegistration()
            self.gotoLookUp()
            self.gotoAdvancedSearch()
        
        filename = os.path.join(directory, '{}-{}.html'.format(subject, number))
        self.saveSearch(subject, number, filename)

    def quit(self):
        logger.info("Quitting...")
        self.driver.quit()
        logger.debug("Done quitting.")

    def __del__(self):
        self.quit()

def main(args):
    scraper = SeleniumScraper(
        term=args.term,
        user_id=args.user_id,
        password=args.password,
    )

    if args.courses in ('*', 'ALL'):
        s, n = 'ALL', ''
        scraper.searchAndSave(s, n, args.courses_dir)
    else:
        for c in args.courses.split(":"):
            parts = c.split()
            if len(parts) == 1:
                s, n = parts[0], ""
            else:
                s, n = parts[:2]
            scraper.searchAndSave(s, n, args.courses_dir)
