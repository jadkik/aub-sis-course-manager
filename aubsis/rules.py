import datetime
from fnmatch import fnmatch

class RuleProcessor(object):
    
    action_rules = {}
    
    def __init__(self, sch, rules):
        self.sch = sch
        self.rules = rules
    
    def run(self):
        for row in self.rules:
            row = row.strip()
            if not row:
                continue

            if row.lstrip().startswith('#'):
                continue

            if row.startswith('\t') or row.startswith(' '*4):
                self.context = None
                continue

            action = row.split()[0]
            args = row[len(action):].strip()

            if action == 'with':
                self.context = RuleContext(self, args)
                continue
            
            try:
                rule_cls = RuleProcessor.action_rules[action]
            except KeyError:
                raise Exception('Unkown rule encountered in `{}`'.format(row))
            else:
                rule = rule_cls(self, self.context, args)
                rule.run()
                self.sch.update()
    
    __context = None
    @property
    def context(self):
        return self.__context
    
    @context.setter
    def context(self, ctx):
        if self.__context is not None:
            self.__context.uninit()
        
        self.__context = ctx
        
        if self.__context is not None:
            self.__context.init()

class RuleContext(object):
    def __init__(self, proc, args):
        self.proc = proc
        self.args = args
    
    def init(self):
        
        self.name = CheckpointRule.new()
        self.rule = CheckpointRule(self.proc, self, self.name)
        self.rule.run()
        
        args = self.args.split()
        self.action = args.pop(0)
        if self.action == 'only':
            if len(args) != 1:
                raise ValueError("Context rule only requires one argument only")
            self.subject = args[0]
        elif self.action == 'either':
            self.subject = args
        elif self.action == 'all':
            self.subject = None
        else:
            raise Exception('fuck you! `{}` `{}`'.format(self.action, self.args))
    
    def uninit(self):
        #pass
        rule = GoToRule(self.proc, self, self.name)
        rule.run()
    
    def filter_cond(self, cond):
        if self.action == 'only':
            return lambda p: (p.course.subject == self.subject and cond(p))
        elif self.action == 'either':
            return lambda p: (p.course.subject in self.subject and cond(p))
        elif self.action == 'all':
            return cond
        else:
            return cond
    
class RuleMeta(type):
    def __new__(cls, name, bases, dct):
        
        instance = super(RuleMeta, cls).__new__(cls, name, bases, dct)
        
        RuleProcessor.action_rules[dct['__action__']] = instance
        
        return instance

class Rule(object):
    __metaclass__ = RuleMeta
    __action__ = None
    
    def __init__(self, proc, ctx, args):
        self.proc = proc
        self.context = ctx
        self.args = args
        
        self.sch = proc.sch
        self.session = self.sch.session
    
    def run(self):
        raise NotImplementedError('This rule cannot run! The run() method is not implemented.')
    
    def filter_periods(self, cond):
        
        if self.context is not None:
            self.sch.rem_periods = set(p for p in self.sch.rem_periods if self.context.filter_cond(cond)(p))
        else:
            self.sch.rem_periods = set(p for p in self.sch.rem_periods if cond(p))

class AddRule(Rule):
    __action__ = 'add'
    
    def run(self):
        crns = self.args.split()
        courses = self.session.query(Course).filter(Course.crn.in_(crns)).all()
        [self.sch.add(c) for c in courses]

class OnlyRule(Rule):
    __action__ = 'only'
    
    def run(self):
        subjects = self.args.split()
        self.filter_periods(lambda p: p.course.subject in subjects)

class MatchRule(Rule):
    __action__ = 'match'
    
    def filter_periods(self, cond):
        newcond = cond if not self.invert_cond else (lambda p: not cond(p))
        return super(MatchRule, self).filter_periods(newcond)
    
    def run(self):
        cond, pattern = self.args.split(None, 1)
        if cond.startswith('!'):
            self.invert_cond = True
            cond = cond[1:]
        else:
            self.invert_cond = False
        
        if cond == 'gt':
            pattern = int(pattern)
            self.filter_periods(lambda p: int(p.course.number[:3]) >= pattern)
        elif cond == 'lt':
            pattern = int(pattern)
            self.filter_periods(lambda p: int(p.course.number[:3]) < pattern)
        elif cond == 'eq':
            self.filter_periods(lambda p: p.course.number == pattern)
        elif cond == 'fn':
            self.filter_periods(lambda p: fnmatch(p.course.number, pattern))
        elif cond == 'in':
            choices = pattern.split()
            self.filter_periods(lambda p: p.course.number in choices)

class MinTime(Rule):
    __action__ = 'after'
    
    def run(self):
        hours, minutes = [int(p.strip()) for p in self.args.split(':')]
        self.filter_periods(lambda p: p.start_time is None or \
                    p.start_time >= datetime.time(hours, minutes)
                )

class MaxTime(Rule):
    __action__ = 'before'
    
    def run(self):
        hours, minutes = [int(p) for p in args.split(':', 2)]
        self.filter_periods(lambda p: p.start_time is None or \
                    p.start_time <= datetime.time(hours, minutes)
                )

class PromptRule(Rule):
    __action__ = 'prompt'
    
    def run(self):
        n = int(self.args)
        for i in range(n):
            if not self.sch.prompt():
                break

class PrintRule(Rule):
    __action__ = 'pprint'
    
    def run(self):
        print 'PRETTY pRiNtInG'
        return self.sch.pprint()

class ValidationRule(Rule):
    __action__ = 'validate'
    
    def run(self):
        return self.sch.validate()

class ByDayRule(Rule):
    __action__ = 'byday'
    
    def run(self):
        return self.sch.by_day()

class PauseRule(Rule):
    __action__ = 'pause'
    
    def run(self):
        try:
            raw_input('Pause...')
            return
        except (KeyboardInterrupt, EOFError):
            return

class FinalizeRule(Rule):
    __action__ = 'finalize'
    
    def run(self):
        return self.sch.finalize()

class CheckpointRule(Rule):
    __action__ = 'checkpoint'
    
    points = {}
    
    @classmethod
    def exists(cls, name):
        return name in cls.points
    
    @classmethod
    def new(cls):
        name = '__checkpoint0'
        i = 0
        while cls.exists(name):
            i += 1
            name = '__checkpoint{}'.format(i)
        return name
    
    def run(self):
        name = self.args
        pt = {  'rem_courses': self.sch.rem_courses.copy(),
                'rem_periods': self.sch.rem_periods.copy()
                }
        
        CheckpointRule.points[name] = pt

class GoToRule(Rule):
    __action__ = 'goto'
    
    def run(self):
        name = self.args
        try:
            pt = CheckpointRule.points[name]
        except KeyError:
            raise KeyError('Checkpoint `{}` not defined'.format(name))
        else:
            self.sch.rem_courses = pt['rem_courses'].copy()
            self.sch.rem_periods = pt['rem_periods'].copy()
