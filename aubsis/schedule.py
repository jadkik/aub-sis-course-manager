import aubsis
from aubsis.rules import RuleProcessor
from aubsis.models import *

from collections import defaultdict
import csv

from sqlalchemy import exists

class Schedule(object):
    
    all_courses = []
    all_periods = []
    periods = set()
    courses = set()
    
    rem_periods = set()
    rem_courses = set()
    
    final = False
    
    week = {d: defaultdict(list) for d in 'MTWRF'}
    
    def __init__(self, session):
        self.session = session
        
        self.all_courses = set(self.session.query(Course).all())
        self.all_periods = set(p for c in self.all_courses for p in c.periods)
        
        self.unique = set((c.subject, c.number) for c in self.all_courses)
        
        self.rem_courses = set(self.all_courses)
        self.rem_periods = set(self.all_periods)
        self.update()
    
    def update(self):
        course_names = set((c.subject, c.number) for c in self.courses)
        
        self.rem_periods = set(p for p in self.rem_periods \
                            if (p.course in self.rem_courses) and \
                                (p.course.subject, p.course.number) not in course_names and \
                                self.all_periods_match(p)
                            )
        self.rem_courses = set(p.course for p in self.rem_periods \
                            if set(p.course.periods).issubset(self.rem_periods)
                            )
        self.rem_periods = set(p for p in self.rem_periods if p.course in self.rem_courses)
    
    def add(self, course):
        periods = set(course.periods)
        self.periods.update(periods)
        self.courses.add(course)
        
        self.update()
    
    def all_periods_match(self, p1):
        return all(self.periods_match(p1, p2) for p2 in self.periods)
    
    def periods_match(self, p1, p2):
        if len(set(p1.days).intersection(set(p2.days))) == 0:
            return True
        return  p1.start_time is None or p2.start_time is None or p1.end_time is None or p2.end_time is None or \
                (p1.end_time < p2.start_time or p2.end_time < p1.start_time)
    
    def finalize(self):
        self.final = True
    
    def validate(self):
        combinations = set(frozenset(p1, p2) for p1 in self.periods for p2 in self.periods if p1 is not p2 and not self.periods_match(p1, p2))
        assert len(combinations) == 0, 'There is a time mismatch with %s' % (combinations,)
    
    def pprint(self):
        print '#'*30
        print
        print 
        
        if len(self.courses) == 0:
            print 'Nothing to show...'
        
        for c in self.courses:
            print c, c.title
            for p in c.periods:
                print '\t', p, ', '.join(p.instructors)
        print 
        print 
    
    def by_day(self):
        times = defaultdict(lambda: defaultdict(lambda: None))
        for p in self.periods:
            if 'TBA' in (p.days, p.start_time, p.end_time):
                # TODO handle TBAs for all schedule and calendar better
                print '{:<4}{:<4}'.format(p.course.subject, p.course.number), 'is TBA'
                continue
            for d in p.days:
                self.week[d][p.start_time, p.end_time] = p
                times[p.start_time][d] = p
        
        print '{:^8}'.format(""),
        for d in 'MTWRF':
            print '{:^8}'.format(d),
        print
        
        for t in sorted(times.iterkeys()):
            print '{:^8}'.format(str(t)),
            for d in 'MTWRF':
                p = times[t][d]
                subject, number = (p.course.subject, p.course.number) if p is not None else ('', '')
                print '{:<4}{:<4}'.format(subject, number),
            print
    
    def save(self, f):
        data = sorted(c.crn for c in self.courses)
        writer = csv.writer(f)
        writer.writerow(data)
    
    def load(self, f):
        reader = csv.reader(f)
        
        for row in reader:
            courses = self.session.query(Course).filter(Course.crn.in_(row)).all()
            [self.add(c) for c in courses]
    
    def db_save(self):
        import aubsis.database
        from aubsis.models import Schedule
        
        courses = list(self.courses)
        sid = Schedule.check(courses)
        if sid is None:
            db_sch = Schedule(courses=courses)
        
            session = aubsis.database.session()
            session.add(db_sch)
            session.commit()
            sid = db_sch.id
        
        print 'Schedule ID:', sid
        return sid
    
    def use_rules(self, rules):
        if not rules:
            while self.prompt():
                pass
            return
        
        ruler = RuleProcessor(self, rules)
        ruler.run()
    
    def pretty(self, ps):
        if ps[0].start_time is None or ps[0].start_time is None:
            return '{p.days}/' \
                    'TBA-TBA' \
                    '\t\t(+{count})\t{instructors}' \
                    .format(p=ps[0],
                            instructors='(None)' if len(ps[0].instructors) == 0 else ', '.join(ps[0].instructors),
                            count=len(ps)-1)
        else:
            return '{p.days}/' \
                    '{p.start_time.hour:0>2}:{p.start_time.minute:0>2}-' \
                    '{p.end_time.hour:0>2}:{p.end_time.minute:0>2}' \
                    '\t\t(+{count})\t{instructors}' \
                    .format(p=ps[0],
                            instructors='(None)' if len(ps[0].instructors) == 0 else ', '.join(ps[0].instructors),
                            count=len(ps)-1)
    
    def prompt(self):
        choices = []

        mapping = defaultdict(lambda: defaultdict(list))
        for p in self.rem_periods:
            if len(p.days) == 1 and \
                    max(len(sib_p.days) for sib_p in p.course.periods) > 1:
                continue
            mapping[p.course.subject, p.course.number][p.days, p.start_time, p.end_time].append(p)
        
        for (subject, number), times in sorted(mapping.iteritems()):
            print 'for', subject, number, '-->', len(times), 'different times'
            for (days, start, end), poss in sorted(times.iteritems()):
                print '\t', len(choices), '= ', self.pretty(poss)
                
                choices.append((subject, number, days, start, end, poss))
        
        if len(choices) == 0:
            self.finalize()
            return False
        
        try:
            choice = input('Select a course to add (0-{})'.format(len(choices)-1))
            choices[choice]
        except (KeyboardInterrupt, EOFError):
            print
            return False
        except:
            self.pprint()
            try:
                raw_input('Continue...')
            except (KeyboardInterrupt, EOFError):
                pass
            return True
        
        subject, number, days, start, end, poss = choices[choice]
        
        print '{c.name}\t{c.title}\n'.format(c=poss[0].course)
        
        for i, p in enumerate(poss):
            assert set(p.course.periods).issubset(self.rem_periods), \
                'There is a course (%s) which has a period in (%s) that is not in the remaining ones!' % \
                    (p.course, p.course.periods)
            print i, '=', p.course, 'at', p.course.periods
            print '\t', 'with', p.instructors
        
        try:
            choice = input('What course(0-{})?'.format(len(poss)-1))
            poss[choice]
        except KeyboardInterrupt:
            print
            return False
        except:
            print 'Ignored.'
            return True
        
        self.add(poss[choice].course)
        
        return True

def main(args):
    session = aubsis.database.session()
    
    sch = Schedule(session)
    
    if args.load is not None:
        sch.load(args.load)
        args.load.close()
        
        sch.finalize()
        
        sch.pprint()
        sch.validate()
        
        sch.by_day()
        
        sch.db_save()
        return
    
    sch.use_rules(args.rules)
    
    if sch.final:
        sch.pprint()
        sch.validate()
        
        sch.by_day()
        
        if args.save:
            sch.save(args.save)
            args.save.close()
        
        sch.db_save()
