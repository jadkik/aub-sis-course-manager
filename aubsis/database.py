from sqlalchemy import create_engine, exc
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import URL

url = None
engine = None
Base = None
Session = None
connection = None
def connect(args):
    global url, engine, Base, Session, connection
    
    if args.db_driver == "mysql":
        query = {'charset': 'utf8', 'use_unicode': '0'}
    else:
        query = None
    
    url = URL(drivername=args.db_driver,
                host=args.db_host, username=args.db_user,
                password=args.db_password, database=args.db_name,
                query=query)

    engine = create_engine(url, echo=args.db_echo)
    Base = declarative_base(bind=engine)
    Session = sessionmaker(bind=engine)

    connection = engine.connect()
    if args.db_driver == "mysql":
        connection.execute("SET NAMES 'utf8'")
        connection.execute("SET CHARACTER SET utf8")

_session = None
def session():
    global _session
    if _session is None:
        _session = Session()
    return _session

def create(args):
    Base.metadata.drop_all()
    Base.metadata.create_all()
