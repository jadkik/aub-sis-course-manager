from bs4 import BeautifulSoup
import glob, re, os
from collections import defaultdict
import datetime

class Course(dict):
    
    # Select 	CRN 	Subj 	Crse 	Sec 	Cmp 	Cred 	Title 	
    # Days 	Time 	Cap 	Act 	Rem 	WL Cap 	WL Act 	WL Rem
    # XL Cap 	XL Act 	XL Rem 	Instructor 	Date (MM/DD) 	Location
    # Attribute
    fields = ['Select', 'CRN', 'Subj', 'Crse', 'Sec', 'Cmp', 'Cred', 'Title',
                'Cap', 'Act', 'Rem', 'WL Cap', 'WL Act', 'WL Rem',
                'XL Cap', 'XL Act', 'XL Rem', 'Attribute']
    
    def __init__(self, cols):
        super(Course, self).__init__()
        self.options = []
        for f in Course.fields:
            self[f] = cols[f]
        self.name = u"{Subj} {Crse}".format(**self)
        self.crn = self["CRN"]
        self.add(cols)
    
    def instructors(self):
        ins = set(i for opt in self.options for i in opt.instructors())
        return list(ins)
    
    def add(self, cols):
        self.options.append(Option(cols))
    
    def __repr__(self):
        return u"{CRN} {Subj} {Crse}".format(**self)
    
    def pretty(self):
        return u"{Subj}{Crse}/{Sec:0>2}".format(**self)

class Option(dict):
    
    fields = ['Days', 'Time', 'Instructor', 'Date (MM/DD)', 'Location']
    
    def __init__(self, cols):
        super(Option, self).__init__()
        for f in Option.fields:
            self[f] = cols[f]
    
    def instructors(self):
        # Remove extra whitespace and punctuation
        ins = set(' '.join(j for j in i.strip().split()) for i in re.findall("[a-zA-Z\.\ \-]{2,}", self["Instructor"]))
        ins.discard('TBA')
        return ins
    
    def __repr__(self):
        return u"{0:>4}{Days:<3} {Time}: {Instructor}".format("", **self)

def parse(filename):
    print 'Parsing', filename
    soup = BeautifulSoup(open(filename, "r"), 'html.parser')
    
    courses = []
    headers = []
    
    term_el = soup.select("div.pagetitlediv table.plaintable "
        "tbody tr td.pldefault div.staticheaders")[0]
    term = term_el.text.splitlines()[1].strip()
    
    for th in soup.select("table.datadisplaytable tbody tr th")[1:]:
        headers.append(th.text)
    
    for tr in soup.select("table.datadisplaytable tbody tr"):
        c = []
        for i, td in enumerate(tr.select("td")):
            c.append(td.text.replace(u'\xa0',u''))
        if len(c) == 0: continue
        if c[1] != "":
            dc = defaultdict(str)
            dc.update(dict(zip(headers, c)))
            courses.append(Course(dc))
        else:
            dc = defaultdict(str)
            dc.update(dict(zip(headers, c[-1:]+c[:-1])))
            courses[-1].add(dc)
    
    return term, courses

def get_courses(courses_dir):
    courses = []
    terms = set()
    for f in glob.glob(os.path.join(courses_dir, "*.htm[l]")):
        if f.startswith('-'):
            continue
        term, cs = parse(f)
        courses.extend(cs)
        terms.add(term)
    return terms, courses

def update_database(args):
    import aubsis.database
    from aubsis.models import Course, Period, Instructor
    
    print 'Parsing...'
    
    terms, courses = get_courses(args.courses_dir)
    
    assert len(terms) == 1, "There is more than one term in this " \
        " course directory"
    
    term = terms.pop()
    candidate_years = None
    
    print term
    
    term_mat = re.match("\\s+([a-zA-Z ]*[a-zA-Z])\\s+([0-9]{2,4})"
        "(?:\\s*\\-\\s*([0-9]{2,4}))?", term)
    if term_mat is not None:
        term_name, term_start_year, term_end_year = term_mat.groups()
        # TODO this won't work in year 2100+ or 3000+
        parse_year = lambda y: None if y is None else \
            (int(y) + 2000 if len(y) == 2 else int(y))
        term_start_year = parse_year(term_start_year)
        term_end_year = parse_year(term_end_year)
        if term_end_year is None:
            term_end_year = term_start_year
    else:
        today = datetime.date.today()
        y = today.year
        term_name, term_start_year, term_end_year = None, y, y
    candidate_years = (term_start_year, term_end_year)
    
    print 'Analysing...'
    
    instructors = set(i for c in courses for i in c.instructors())
    instructors = list(instructors)
    instructor_map = {}
    db_instructors = []
    for i in instructors:
        db_i = Instructor(name=i)
        instructor_map[i] = db_i
        db_instructors.append(db_i)
    
    db_courses = []
    db_periods = []
    for c in courses:
        db_c = Course(crn=c.crn, subject=c['Subj'], section=c['Sec'],
                number=c['Crse'], title=c['Title'])
        db_courses.append(db_c)
        print '>', db_c
        for opt in c.options:
            start, end = parse_times(opt['Time'])
            start_date, end_date = parse_dates(candidate_years, opt['Date (MM/DD)'])
            db_p = Period(course=db_c, days=opt['Days'], start_time=start, end_time=end,
                start_date=start_date, end_date=end_date,
                location=opt['Location'], ins=[instructor_map[i] for i in opt.instructors()])
            db_periods.append(db_p)
    
    session = aubsis.database.session()
    session.add_all(db_instructors)
    session.add_all(db_courses)
    session.add_all(db_periods)
    session.commit()

def parse_times(ts):
    # 10:00 am-10:50 am
    try:
        p = []
        for t in ts.split('-'):
            a = 12 if 'pm' in t else 0
            h, m = [int(hm[:2], 10) for hm in t.strip().split(':')]
            if h == 12:
                h = 0
            p.append(datetime.time(h+a, m))
        return p[0], p[1]
    except:
        return None, None

def parse_dates(years, ds):
    # 09/04-12/21
    try:
        p = []
        for d in ds.split('-'):
            month, day = [int(n.strip(), 10) for n in d.split('/')]
            p.append(datetime.date(years[0], month, day))
        if p[1] < p[0]:
            p[1] = p[1].replace(year=years[1])
        assert p[0] < p[1], "Term years (%d - %d) are invalid" % years
        return p[0], p[1]
    except:
        return None, None

def main(args):
    terms, courses = get_courses(args.courses_dir)
    
    assert len(terms) == 1, "There is more than one term in this " \
        " course directory"
    
    by_name = defaultdict(list)
    [by_name[c.name].append(c) for c in courses]

    print '  '.join(by_name.keys())
    in_course = raw_input("Course?")

    if in_course not in by_name:
        all_ins = []
        for c in courses:
            all_ins.extend(i for i in c.instructors() if i not in all_ins)
        print "Instructors:", ",".join(all_ins)
        for name, cs in by_name.iteritems():
            print name
            for c in cs:
                print "{0:>8}/CRN: {1} - {2}".format("", c.crn, c['Sec'], c['Title'])
                for opt in c.options:
                    print opt
    else:
        cs = by_name[in_course]
        for c in cs:
            print "{0:>8}/CRN: {1} - {2}\t{3}".format("", c.crn, c['Sec'], c['Title'])
            for opt in c.options:
                print opt
