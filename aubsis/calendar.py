from reportlab.lib import colors
from reportlab.pdfgen.canvas import Canvas
from reportlab.lib.pagesizes import A4, letter, landscape, portrait
import reportlab.lib.pagesizes
from reportlab.lib.units import inch
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, \
                                                 Paragraph, Spacer

from reportlab.lib.styles import ParagraphStyle, getSampleStyleSheet
from reportlab.lib.enums import TA_LEFT, TA_CENTER, TA_RIGHT

from babel.dates import format_date, format_time, get_day_names

import string, random
import datetime as dt

import subprocess, sys, os

import aubsis
from aubsis.models import Schedule

AUTHOR = 'AUBSIS'
COLORS = {c: getattr(colors, c) for c in dir(colors) if
    isinstance(getattr(colors, c), colors.Color) and
    c[:1] in string.lowercase and
    colors.colorDistance(getattr(colors, c), colors.black) >= 0.8 and
    colors.colorDistance(getattr(colors, c), colors.white) >= 0.25
}

def openfile(filepath):
    if sys.platform.startswith('darwin'):
        subprocess.call(('open', filepath))
    elif os.name == 'nt':
        os.startfile(filepath)
    elif os.name == 'posix':
        subprocess.call(('xdg-open', filepath))

class Calendar(object):
    
    period_template = """{c.name} - {c.section}
{p.start_time:%I:%M} - {p.end_time:%I:%M %p}
@ {p.location}
{c.crn} Class
w/ {instructors}
"""
    
    def __init__(self, filename, schedule, title=None, author=None, locale='en_US', pagesize=landscape(A4), grayscale=False):
        self.grayscale = grayscale
        self.pagesize = pagesize
        
        self.canvas = Canvas(filename, pagesize=self.pagesize)
        self.canvas.setTitle(title)
        self.canvas.setAuthor(author)

        self.filename = filename
        self.title = title
        self.author = author
        self.locale = locale

        self.schedule = schedule

    def open_native(self):
        if os.path.isfile(self.filename):
            return openfile(self.filename)

    def calc_t(self, tm):
        return tm.hour * 60 + tm.minute
    
    def calc_hm(self, t):
        return divmod(t, 60)
    
    def calc_tm(self, t):
        return dt.time(*self.calc_hm(t))

    def calc_t_start(self, tm):
        t = max(0, self.calc_t(tm)-60)
        rh, rm = self.calc_hm(t)
        return rh * 60

    def calc_t_end(self, tm):
        t  = min(24 * 60, self.calc_t(tm)+60)
        rh, rm = self.calc_hm(t)
        if rm == 0:
            return rh * 60
        else:
            return min(24, rh+1) * 60

    __dayindex = 'MTWRFSU'
    __daymap = None
    def daylabel(self, d):
        if self.__daymap is None:
            self.__daymap = get_day_names('wide', 'stand-alone', self.locale)
        return self.__daymap[self.__dayindex.index(d)]

    def build(self, start=None, end=None, days=None):
        """
        Draw the table on the canvas.
        """
        if days is None:
            days = 'UMTWRF'
            day_range = set([days.index(d) for p in self.schedule.periods for d in p.days if p.days != 'TBA'])
            days = days[min(day_range):max(day_range)+1]
        
        headers = [self.daylabel(d) for d in days]
        
        if start is None:
            start = min(p.start_time for p in self.schedule.periods if p.start_time is not None)
        start = self.calc_t_start(start)
        
        if end is None:
            end = max(p.end_time for p in self.schedule.periods if p.end_time is not None)
        end = self.calc_t_end(end)
        
        # Calculate num rows and columns
        rows = int((end-start) / 30.0 + 1)
        cols = len(headers)
        
        margin = 0.5*inch
        outerwidth, outerheight = self.pagesize
        innerwidth, innerheight = outerwidth-2*margin, outerheight-2*margin
        width, height = outerwidth, outerheight

        font_size = 12

        ######
        # Initial state
        self.canvas.saveState()
        ######

        # Start drawing
        ###############
        
        ######
        # Font and translation
        self.canvas.setFont('Helvetica', font_size)
        
        self.canvas.translate(margin, margin)
        width, height = innerwidth, innerheight
        
        self.canvas.saveState()
        ######
        
        # First column and row
        first_rowheight = 0.25 * inch
        first_colwidth = 0.5 * inch
        
        # Make sure the time would fit in that size
        test_time = format_time(dt.time(23, 59, 59), format='short', locale=self.locale)
        w = self.canvas.stringWidth(test_time)
        r = 0.9
        first_colwidth = max(first_colwidth, w * (2.0/r - 1))
        
        # Rest of the columns and rows
        colwidth = (width-first_colwidth)/cols
        rowheight = (height-first_rowheight)/rows
        
        # Draw the general calendar grid
        
        # Gray Lines (odd ones)
        xs = [first_colwidth, width]
        ys = [height - (first_rowheight + y*rowheight) for y in xrange(1, rows, 2)]
        lines = [(xs[0], y, xs[1], y) for y in ys]
        
        self.canvas.saveState()
        self.canvas.setStrokeGray(0.5)
        self.canvas.lines(lines)
        self.canvas.restoreState()
        
        # Black Lines (even ones)
        xs = [first_colwidth, width]
        ys = [height - (first_rowheight + y*rowheight) for y in xrange(0, rows, 2)]
        lines = [(xs[0], y, xs[1], y) for y in ys[1:]]
        
        self.canvas.lines(lines)
        
        # Vertical Lines
        xs = [first_colwidth + x*colwidth for x in xrange(1, cols)]
        ys = [height - first_rowheight, 0]
        lines = [(x, ys[0], x, ys[1]) for x in xs]
        
        self.canvas.saveState()
        self.canvas.setLineWidth(0.5)
        self.canvas.setDash(3, 3)
        self.canvas.lines(lines)
        self.canvas.restoreState()
        
        """
        # Draw the header grid
        xs = [first_colwidth] + [first_colwidth+(x+1)*colwidth for x in xrange(cols)]
        ys = [height, height-first_rowheight]
        
        self.canvas.grid(xs, ys)
        """
        
        # Draw the first column background
        self.canvas.saveState()
        
        if self.grayscale:
            self.canvas.setFillColor(colors.black)
            self.canvas.setFillGray(0.9)
        else:
            self.canvas.setFillColor(colors.lightblue)
        self.canvas.rect(0, 0, first_colwidth, height, stroke=0, fill=1)
        
        self.canvas.restoreState()
        
        # Draw the header background
        self.canvas.saveState()
        
        if self.grayscale:
            self.canvas.setFillColor(colors.black)
            self.canvas.setFillGray(0.6)
        else:
            self.canvas.setFillColor(colors.dodgerblue)
        self.canvas.rect(0, height-first_rowheight, width, first_rowheight, stroke=0, fill=1)
        
        self.canvas.restoreState()
        
        # Draw the bounding box
        self.canvas.saveState()
        
        if self.grayscale:
            self.canvas.setStrokeColor(colors.black)
            self.canvas.setStrokeGray(0.6)
        else:
            self.canvas.setStrokeColor(colors.dodgerblue)
        self.canvas.setLineWidth(0.05*first_colwidth)
        self.canvas.rect(0, 0, width, height, stroke=1, fill=0)
        
        self.canvas.restoreState()
        
        # Draw the header text
        for i, cell in enumerate(headers):
            w = self.canvas.stringWidth(cell)
            text = self.canvas.beginText(first_colwidth+i*colwidth+(colwidth-w)/2, height-first_rowheight*0.75)
            text.textLine(cell)
            self.canvas.drawText(text)
        
        # Draw the title text
        w = self.canvas.stringWidth(self.title)
        text = self.canvas.beginText(0, height+margin/4.0)
        text.textLine(self.title)
        self.canvas.drawText(text)
        
        # Draw the calendar period in the other corner
        calendar_start = min(p.start_date for p in self.schedule.periods if p.start_date is not None)
        calendar_end = max(p.end_date for p in self.schedule.periods if p.end_date is not None)
        calendar_period = u'[{} - {}]'.format(
            format_date(calendar_start, 'medium', self.locale),
            format_date(calendar_end, 'medium', self.locale)
        )
        
        w = self.canvas.stringWidth(calendar_period)
        text = self.canvas.beginText(width-w, height+margin/4.0)
        text.textLine(calendar_period)
        self.canvas.drawText(text)
        
        """
        # Draw an outline box for the whole thing
        self.canvas.saveState()
        self.canvas.setLineWidth(2)
        self.canvas.grid([first_colwidth, width], [height - first_rowheight, 0])
        self.canvas.restoreState()
        """
        
        # Draw the first column text
        for t in xrange(start, end, 60):
            cell = format_time(self.calc_tm(t), format='short', locale=self.locale)
            i = (t-start)/30
            w = self.canvas.stringWidth(cell)
            text = self.canvas.beginText(0.9*first_colwidth-w, height - (first_rowheight + rowheight*i + font_size))
            text.textLine(cell)
            self.canvas.drawText(text)
        
        # Draw the periods
        self.canvas.saveState()
        self.canvas.translate(first_colwidth, 0)
        font_size -= 2
        height -= first_rowheight
        
        if not self.grayscale:
            course_colors = {}
            all_colors = COLORS.values()
            random.shuffle(all_colors)
        
        for p in self.schedule.periods:
            if p.start_time is None or p.end_time is None or p.days == 'TBA':
                continue
            p_start = self.calc_t(p.start_time)
            p_end = self.calc_t(p.end_time)
            
            if self.grayscale:
                color = colors.black
            else:
                try:
                    color = course_colors[p.course_id]
                except KeyError:
                    course_colors[p.course_id] = color = all_colors.pop()
            
            for d in p.days:
                try:
                    x = days.index(d)
                except ValueError:
                    # This day is not in the list. Ignore it.
                    continue
                y0, y1 = p_start-start, p_end-start
                if not p.instructors:
                        instructors = '[N/A]'
                elif len(p.instructors) == 1:
                        instructors = p.instructors[0]
                else:
                        instructors = p.instructors[0] + ' &co.'
                cell = self.period_template.format(c=p.course, p=p,
                        instructors=instructors)
                
                self.canvas.saveState()
                
                tx = colwidth * x
                ty = height - (rowheight * y0/30)
                th = (y1-y0)/30.0 * rowheight
                tw = colwidth
                self.canvas.translate(tx, ty)
                height, _height = th, height
                
                self.canvas.setFillColor(color)
                if self.grayscale:
                    self.canvas.setFillGray(0.8)
                
                self.canvas.roundRect(
                    font_size/4.0, -th, # x, y
                    tw-font_size/2.0, th, # w, h
                    font_size/2.0, # radius
                    stroke=0, fill=1)
                
                self.canvas.setFillColor(colors.black)
                self.canvas.setFont('Helvetica', font_size)
                
                text = self.canvas.beginText(font_size/2.0, -font_size)
                for line in cell.splitlines()[:int(th/font_size - 1)]:
                    text.textLine(line)
                self.canvas.drawText(text)
                
                height = _height
                self.canvas.restoreState()
        
        height += first_rowheight
        font_size += 2
        self.canvas.restoreState()

        ######
        # Font and translation
        self.canvas.restoreState()
        
        width, height = outerwidth, outerheight
        ######
        
        ##############
        # Stop Drawing
        
        ######
        # Initial State
        self.canvas.restoreState()
        ######
        
        # Finalize the page
        self.canvas.showPage()
        
        # Save the canvas to the file
        self.canvas.save()

def main(args):
    session = aubsis.database.session()
    
    if args.pagesize not in ("A4", "letter"):
        args.pagesize = "A4"
    pagesize = getattr(reportlab.lib.pagesizes, args.pagesize)
    
    if args.orientation == "landscape":
        pagesize = landscape(pagesize)
    else:
        pagesize = portrait(pagesize)
    
    sch = session.query(Schedule).filter_by(id=args.schedule_id).one()
    report = Calendar(
        filename=args.output,
        title=args.title,
        author=args.author,
        locale=args.locale,
        schedule=sch,
        pagesize=pagesize,
        grayscale=args.grayscale,
    )
    report.build(days=args.days)
    if args.open_native:
        report.open_native()
