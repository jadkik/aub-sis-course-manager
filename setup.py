from setuptools import setup, find_packages

setup(
      name="AUBsis",
      version="0.1",
      packages=find_packages(),
      scripts=['sis'],
      
      zip_safe=True,
      
      install_requires=[
        'sqlalchemy>=0.8',
        'reportlab',
        'selenium',
        'beautifulsoup4',
        'iCalendar',
        'babel>=1.3'
      ],
      
      author='Jad Kik',
      author_email='jadkik94@gmail.com',
      description='AUB SIS course scheduler',
      license='GPLv3',
      url='http://www.aub.edu.lb/banner'
)
