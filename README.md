# AUB SIS Manager

It manages the courses of AUB SIS.

There are tools to scrape the SIS to get the list of courses, add them to the database, schedule the courses, visualise them,
print the schedule on a PDF calendar.

Soon, there will be a web interface and a desktop interface. Now, it is all done via a CLI; so eveerything is passed
by command line arguments, and nothing from the standard input.

It uses a database, so by default it will use sqlite on a file `aubsis.sqlite` in the current working directory. You can 
specify (also via command line arguments) which db driver/name/user/password/host to use.

Example usage:

    # Create the virtual env
    virtualenv sis
    # ... download requirements.txt ...
    cd sis
    source bin/activate
    # Install everything
    pip install -r requirements.txt

    # Download chromedriver for your version of chrome
    # For example, if your Chrome version is 79
    # Go to: https://sites.google.com/a/chromium.org/chromedriver/downloads
    # Click on 79: https://chromedriver.storage.googleapis.com/index.html?path=79.0.3945.36/
    # Download and unzip the zip file
    wget https://chromedriver.storage.googleapis.com/79.0.3945.36/chromedriver_linux64.zip
    unzip chromedriver_linux64.zip
    # Add it to the path (need to do this in your current shell everytime)
    export PATH="$(pwd)/chromedriver_linux64/":"$PATH"
    
    # Clear/Create the database
    sis clear
    mkdir data
    mkdir data/courses
    
    # Get the files from the sis website website
    sis scraper -c data/courses -u 201300xxx -p PASSWORD -t "Fall 2013-2014" -s "EECE 3:SOAN:PHYS 210L"
    
    # Update the database with the newly downloaded data
    sis update -c data/courses
    
    # Set up a schedule and save the CRNs to a csv file
    sis schedule -s data/schedule.csv -r data/rules.cfg
    
    # It will tell you what the schedule ID is when you are done
    # Then you generate the calendar
    sis calendar -s abc23fec76aeb12fde98aec17bdf3455 -o data/schedule.pdf -p A4 -r landscape --grayscale
    
    # You can also export it in the iCalendar format to import it in your favorite calendar application
    # Tested it with ownCloud, and it's great!
    sis export -s abc23fec76aeb12fde98aec17bdf3455 -o data/schedule.ics -t LB
    # Note: You do not have to specify the timezone. If you don't all the dates will not carry any information on timezone
    #       You can also specify the timezone by country code (e.g. LB for Lebanon) or by name (e.g. Asia/Beirut) or set it to UTC

    # Later, to load the schedule
    sis schedule -l data/schedule.csv
    # It will also tell you what the ID is, in case you need to re-generate the calendar

To set up a schedule, you need to have a rules file, which lets you chose conveniently a schedule. Here's a sample:

    # File 1: eece-only-schedule.cfg
    only EECE
    after 08:00
    
    # Only 3 EECE courses
    prompt 3

    # pretty print the thing
    pprint
    pause
    # print the day-by-day calendar
    byday

    # if you don't like something and don't want to save Ctrl-C at a `pause`
    pause
    # done
    finalize

    # END FILE eece-only-schedule.cfg

    # File 2: sample-fall.cfg
    
    with all
        after 07:00
    

    checkpoint start

    with only EECE
        match fn 3*
        match !eq 312
        match !eq 312L
        match !eq 321
        match !eq 340
        match !eq 380
        match !eq 370

    # We want EECE 310/310L/320/330
        prompt 4
    
    # Restores the state/context at start
    goto start

    with only PHYS
        match eq 210L
        
    # We want to choose which 210L to take after the EECE courses
        prompt 1

    
    goto start

    with only SOAN
        match eq 201

    # Now choose SOAN 201
        prompt 1

    byday
    pause
    
    finalize

And that's it!
